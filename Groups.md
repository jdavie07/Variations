# Variation Groups

Summaries of related variations are grouped together. This page lists the groups with the images of variations in the groups. Click on the group name to go to the group, or an image to go to the variation it represents. (Not all variations have images, and some variations have more than one image.)

## [Attractors](attractors/attractors.md#Attractors)

These variations use the formulas for strange attractors.

[![](attractors/clifford-1.png)](attractors/attractors.md#clifford_js)
[![](attractors/gingerbread_man-1.png)](attractors/attractors.md#gingerbread_man)
[![](attractors/gumowski_mira-1.png)](attractors/attractors.md#gumowski_mira)
[![](attractors/henon-1.png)](attractors/attractors.md#henon)
[![](attractors/hopalong-1.png)](attractors/attractors.md#hopalong)
[![](attractors/hopalong-2.png)](attractors/attractors.md#hopalong)
[![](attractors/lorenz-1.png)](attractors/attractors.md#lorenz_js)
[![](attractors/lozi-1.png)](attractors/attractors.md#lozi)
[![](attractors/macmillan-1.png)](attractors/attractors.md#macmillan)
[![](attractors/pdj-1.png)](attractors/attractors.md#pdj)
[![](attractors/pdj3D-1.png)](attractors/attractors.md#pdj3D)
[![](attractors/sattractor3D-1.png)](attractors/attractors.md#sattractor3D)
[![](attractors/sattractor3D-2.png)](attractors/attractors.md#sattractor3D)
[![](attractors/sattractor-1.png)](attractors/attractors.md#sattractor_js)
[![](attractors/sattractor-2.png)](attractors/attractors.md#sattractor_js)
[![](attractors/svensson-1.png)](attractors/attractors.md#svensson_js)
[![](attractors/threeply-1.png)](attractors/attractors.md#threeply)
[![](attractors/threeply-2.png)](attractors/attractors.md#threeply)

## [Blobs](blobs/blobs.md#Blobs)
Variations that deform the plane by pinching it towards the origin, making a blob-like shape.

[![](blobs/blob-1.png)](blobs/blobs.md#blob)
[![](blobs/blob-2.png)](blobs/blobs.md#blob)
[![](blobs/blob2-1.png)](blobs/blobs.md#blob2)
[![](blobs/blob2-2.png)](blobs/blobs.md#blob2)
[![](blobs/blob3D-1.png)](blobs/blobs.md#blob3D)
[![](blobs/blob3D-2.png)](blobs/blobs.md#blob3D)
[![](blobs/flower_db-1.png)](blobs/blobs.md#flower_db)
[![](blobs/flower_db-2.png)](blobs/blobs.md#flower_db)
[![](blobs/rose-1.png)](blobs/blobs.md#rose)
[![](blobs/rose-2.png)](blobs/blobs.md#rose)

## [Blurry](blurry/blurry.md#Blurry)
Variations that add different types of blurriness.

[![](blurry/blur_linear-1.png)](blurry/blurry.md#blur_linear)
[![](blurry/blur_pixelize-1.png)](blurry/blurry.md#blur_pixelize)
[![](blurry/blur_zoom-1.png)](blurry/blurry.md#blur_zoom)
[![](blurry/exblur-1.png)](blurry/blurry.md#exblur)
[![](blurry/noise-1.png)](blurry/blurry.md#noise)
[![](blurry/pixel_flow-1.png)](blurry/blurry.md#pixel_flow)
[![](blurry/radial_blur-1.png)](blurry/blurry.md#radial_blur)

## [Boarders](boarders/boarders.md#Boarders)

Divide the plane into squares or hexagons with borders.

[![](boarders/boarders2-1.png)](boarders/boarders.md#boarders2--pre_boarders2)
[![](boarders/xtrb-1.png)](boarders/boarders.md#xtrb)

## [Complex Power](cpow/cpow.md#Complex-Power)

Treats 2D points as complex numbers and raises them to a complex power that can be specified in various ways.

[![](cpow/cpow-1.png)](cpow/cpow.md#cpow)
[![](cpow/cpow2-1.png)](cpow/cpow.md#cpow2)
[![](cpow/cpow3-1.png)](cpow/cpow.md#cpow3)
[![](cpow/cpow3_wf-1.png)](cpow/cpow.md#cpow3_wf)
[![](cpow/escher-1.png)](cpow/cpow.md#escher)

## [Disc](disc/disc.md#Disc)

Variations that use polar coordinates and swap the ρ and θ values.

[![](disc/disc-1.png)](disc/disc.md#disc-1)
[![](disc/disc2-1.png)](disc/disc.md#disc2)
[![](disc/disc3-1.png)](disc/disc.md#disc3)
[![](disc/disc3d-1.png)](disc/disc.md#disc3d)
[![](disc/edisc-1.png)](disc/disc.md#edisc)
[![](disc/fdisc-1.png)](disc/disc.md#fdisc)
[![](disc/idisc-1.png)](disc/disc.md#idisc)
[![](disc/wdisc-1.png)](disc/disc.md#wdisc)

## [Flippers](filppers/flippers.md#Flippers)
Variations that divide up the plane in various ways and flip or rotate the parts.

[![](flippers/fan2-1.png)](flippers/flippers.md#fan2)
[![](flippers/flipcircle-1.png)](flippers/flippers.md#flipcircle)
[![](flippers/flipy-1.png)](flippers/flippers.md#flipy)
[![](flippers/glitchy1-1.png)](flippers/flippers.md#glitchy1)
[![](flippers/lazysensen-1.png)](flippers/flippers.md#lazysensen)
[![](flippers/minkowscope-1.png)](flippers/flippers.md#minkowscope)
[![](flippers/oscilloscope-1.png)](flippers/flippers.md#oscilloscope)
[![](flippers/oscilloscope2-1.png)](flippers/flippers.md#oscilloscope2)
[![](flippers/rectangles-1.png)](flippers/flippers.md#rectangles)
[![](flippers/scrambly-1.png)](flippers/flippers.md#scrambly)
[![](flippers/split-1.png)](flippers/flippers.md#split)
[![](flippers/tqmirror-1.png)](flippers/flippers.md#tqmirror)
[![](flippers/trade-1.png)](flippers/flippers.md#trade)

## [Inversion](inversion/inversion.md#Inversion)

Variations that invert across various shapes, such as the unit circle.

[![](inversion/d_spherical-1.png)](inversion/inversion.md#d_spherical)
[![](inversion/inversion-1.png)](inversion/inversion.md#inversion-1)
[![](inversion/octagon-1.png)](inversion/inversion.md#octagon)
[![](inversion/octagon-2.png)](inversion/inversion.md#octagon)
[![](inversion/spherical-1.png)](inversion/inversion.md#spherical)

## [Julia](julia/julia.md#Julia)

Variations based on polynomial Julia sets.

[![](julia/eJulia-1.png)](julia/julia.md#eJulia)
[![](julia/julia3D-1.png)](julia/julia.md#julia3D)
[![](julia/julia3Dq-1.png)](julia/julia.md#julia3Dq)
[![](julia/julia3Dz-1.png)](julia/julia.md#julia3Dz)
[![](julia/juliac-1.png)](julia/julia.md#juliac)
[![](julia/juliacomplex-1.png)](julia/julia.md#juliacomplex)
[![](julia/julian-1.png)](julia/julia.md#julian)
[![](julia/julian-2.png)](julia/julia.md#julian)
[![](julia/julian2-1.png)](julia/julia.md#julian2)
[![](julia/julian2-2.png)](julia/julia.md#julian2)
[![](julia/julian2dc-1.png)](julia/julia.md#julian2dc)
[![](julia/julian3Dx-1.png)](julia/julia.md#julian3Dx)
[![](julia/juliaNab-1.png)](julia/julia.md#juliaNab)
[![](julia/juliaq-1.png)](julia/julia.md#juliaq)
[![](julia/npolar-1.png)](julia/julia.md#npolar)
[![](julia/npolar-2.png)](julia/julia.md#npolar)
[![](julia/phoenix_julia-1.png)](julia/julia.md#phoenix_julia)

## [Linear](linear/linear.md#Linear)

Variations that just copy the input, some with minor modification.

[![](linear/dc_linear-1.png)](linear/linear.md#dc_linear)
[![](linear/dc_linear-2.png)](linear/linear.md#dc_linear)
[![](linear/linear-1.png)](linear/linear.md#linear-1)
[![](linear/linearT-1.png)](linear/linear.md#linearT)

## [L-Systems](lsystems/lsystems.md#L-Systems)

Variations that use L-Systems to generate shapes.

[![](lsystems/lsystem-1.png)](lsystems/lsystems.md#lsystem_js)
[![](lsystems/lsystem3D-1.png)](lsystems/lsystems.md#lsystem3D_js)

## [Maurer Lines](maurerlines/maurerlines.md#Maurer-Lines)

Maurer lines extend the Maurer rose with different types of curves, rendering methods, and coloring.

[![](maurerlines/maurer_lines-1.png)](maurerlines/maurerlines.md#maurer_lines)
[![](maurerlines/maurer_lines-2.png)](maurerlines/maurerlines.md#maurer_lines)

## [Meshes](meshes/meshes.md#Meshes)
Variations that generate meshes, three dimensional shapes made from connected triangles. These are all blur variations that ignore their inputs.

[![](meshes/obj_mesh_primitive_wf-1.png)](meshes/meshes.md#obj_mesh_primitive_wf)
[![](meshes/obj_mesh_wf-1.png)](meshes/meshes.md#obj_mesh_wf)
[![](meshes/sattractor3D-1.png)](meshes/meshes.md#sattractor3D)
[![](meshes/sattractor3D-2.png)](meshes/meshes.md#sattractor3D)
[![](meshes/terrain3D-1.png)](meshes/meshes.md#terrain3D)

## [Plotting Variations](plotting/plotting.md#Plotting-variations)

These variations are all blurs that plot formulas in two or three dimensions. They allow entering custom formulas, but also come with presets for ease of use.

[![](plotting/isosfplot3d-1.png)](plotting/plotting.md#isosfplot3d_wf)
[![](plotting/isosfplot3d-2.png)](plotting/plotting.md#isosfplot3d_wf)
[![](plotting/parplot2d-1.png)](plotting/plotting.md#parplot2d_wf)
[![](plotting/parplot2d-2.png)](plotting/plotting.md#parplot2d_wf)
[![](plotting/polarplot2d-1.png)](plotting/plotting.md#polarplot2d_wf)
[![](plotting/polarplot2d-2.png)](plotting/plotting.md#polarplot2d_wf)
[![](plotting/polarplot3d-1.png)](plotting/plotting.md#polarplot3d_wf)
[![](plotting/polarplot3d-2.png)](plotting/plotting.md#polarplot3d_wf)
[![](plotting/yplot2d-1.png)](plotting/plotting.md#yplot2d_wf)
[![](plotting/yplot2d-2.png)](plotting/plotting.md#yplot2d_wf)
[![](plotting/yplot3d-1.png)](plotting/plotting.md#yplot3d_wf)
[![](plotting/yplot3d-2.png)](plotting/plotting.md#yplot3d_wf)

## [Polar](polar/polar.md#Polar)

Variations that switch between polar or log-polar and rectangular coordinates.

[![](polar/invpolar-1.png)](polar/polar.md#invpolar)
[![](polar/polar-1.png)](polar/polar.md#polar-1)
[![](polar/polar-2.png)](polar/polar.md#polar-1)
[![](polar/polar2-1.png)](polar/polar.md#polar2)
[![](polar/polar2-2.png)](polar/polar.md#polar2)
[![](polar/unpolar-1.png)](polar/polar.md#unpolar)
[![](polar/unpolar-2.png)](polar/polar.md#unpolar)

## [Polar Shapes](polarshapes/polarshapes.md#Half-Blurs)
Variations that generate specific shapes using polar coordinates.

[![](polarshapes/cannabiscurve-1.png)](polarshapes/polarshapes.md#cannabiscurve_wf)
[![](polarshapes/cloverleaf-1.png)](polarshapes/polarshapes.md#cloverleaf_wf)
[![](polarshapes/conic-1.png)](polarshapes/polarshapes.md#conic--conic2)
[![](polarshapes/conic-2.png)](polarshapes/polarshapes.md#conic--conic2)
[![](polarshapes/shape-1.png)](polarshapes/polarshapes.md#shape)
[![](polarshapes/shape-2.png)](polarshapes/polarshapes.md#shape)

## [Reshapers](reshapers/reshapers.md#Reshapers)

Variations that reshape the input (for example, turn a circle into a square or other polygon).

[![](reshapers/butterfly-1.png)](reshapers/reshapers.md#butterfly)
[![](reshapers/circlize2-1.png)](reshapers/reshapers.md#circlize2)
[![](reshapers/circlize2-2.png)](reshapers/reshapers.md#circlize2)
[![](reshapers/ngon-1.png)](reshapers/reshapers.md#ngon)
[![](reshapers/squarize-1.png)](reshapers/reshapers.md#squarize)
[![](reshapers/prepost_circlize-1.png)](reshapers/reshapers.md#prepost_circlize)
[![](reshapers/super_shape-1.png)](reshapers/reshapers.md#super_shape)
[![](reshapers/xheart-1.png)](reshapers/reshapers.md#xheart)

## [Rose Curve](rosecurve/rosecurve.md#Variations-Based-on-the-Rose-Curve)

Variations based on the rose or rhodonea curve.

[![](rosecurve/epispiral-1.png)](rosecurve/rosecurve.md#epispiral)
[![](rosecurve/epispiral_wf-1.png)](rosecurve/rosecurve.md#epispiral_wf)
[![](rosecurve/flower-1.png)](rosecurve/rosecurve.md#flower)
[![](rosecurve/flower-2.png)](rosecurve/rosecurve.md#flower)
[![](rosecurve/flower3D-1.png)](rosecurve/rosecurve.md#flower3D)
[![](rosecurve/maurer_rose-1.png)](rosecurve/rosecurve.md#maurer_rose)
[![](rosecurve/maurer_rose-2.png)](rosecurve/rosecurve.md#maurer_rose)
[![](rosecurve/pRose3D-1.png)](rosecurve/rosecurve.md#pRose3D)
[![](rosecurve/pRose3D-2.png)](rosecurve/rosecurve.md#pRose3D)
[![](rosecurve/rhodonea-1.png)](rosecurve/rosecurve.md#rhodonea)
[![](rosecurve/rhodonea-2.png)](rosecurve/rosecurve.md#rhodonea)
[![](rosecurve/rose_wf-1.png)](rosecurve/rosecurve.md#rose_wf)

## [Shapes](shapes/shapes.md#Shapes)

Variations that ignore the input and generate specific shapes.

[![](shapes/blur-1.png)](shapes/shapes.md#blur)
[![](shapes/blur_heart-1.png)](shapes/shapes.md#blur_heart)
[![](shapes/blur3D-1.png)](shapes/shapes.md#blur3D--pre_blur3D)
[![](shapes/chrysanthemum-1.png)](shapes/shapes.md#chrysanthemum)
[![](shapes/circleblur-1.png)](shapes/shapes.md#circleblur)
[![](shapes/gaussian_blur-1.png)](shapes/shapes.md#gaussian_blur)
[![](shapes/nblur-1.png)](shapes/shapes.md#nBlur)
[![](shapes/nblur-2.png)](shapes/shapes.md#nBlur)
[![](shapes/pie-1.png)](shapes/shapes.md#pie)
[![](shapes/pie3D-1.png)](shapes/shapes.md#pie3D)
[![](shapes/primitives_wf-1.png)](shapes/shapes.md#primitives_wf)
[![](shapes/sineblur-1.png)](shapes/shapes.md#sineblur)
[![](shapes/square-1.png)](shapes/shapes.md#square)
[![](shapes/square3D-1.png)](shapes/shapes.md#square3D)
[![](shapes/starblur-1.png)](shapes/shapes.md#starblur)
[![](shapes/starblur-2.png)](shapes/shapes.md#starblur)
[![](shapes/superShape3d-1.png)](shapes/shapes.md#superShape3d)
[![](shapes/superShape3d-2.png)](shapes/shapes.md#superShape3d)
[![](shapes/triangle-1.png)](shapes/shapes.md#triangle)
[![](shapes/waveblur-1.png)](shapes/shapes.md#waveblur_wf)
[![](shapes/xheart_blur-1.png)](shapes/shapes.md#xheart_blur_wf)

## [Shredders](shredders/shredders.md#Shredders)

Variations that shred the plane in various ways.

[![](shredders/checks-1.png)](shredders/shredders.md#checks)
[![](shredders/shredded-1.png)](shredders/shredders.md#shredded)
[![](shredders/shredlin-1.png)](shredders/shredders.md#shredlin)
[![](shredders/shredrad-1.png)](shredders/shredders.md#shredrad)

## [Splitters](splitters/splitters.md#Splitters)

Variations that split the flame in various ways

[![](splitters/circlesplit-1.png)](splitters/splitters.md#circlesplit)
[![](splitters/circus-1.png)](splitters/splitters.md#circus)
[![](splitters/corners-1.png)](splitters/splitters.md#corners)
[![](splitters/separation-1.png)](splitters/splitters.md#separation)
[![](splitters/spligon-1.png)](splitters/splitters.md#spligon)
[![](splitters/splits-1.png)](splitters/splitters.md#splits)
[![](splitters/splits3D-1.png)](splitters/splitters.md#splits3D)

## [Stamps](stamps/stamps#Stamps)

Variations that create colored stamps with interesting designs.

[![](stamps/dc_acrilic-1.png)](stamps/stamps.md#dc_acrilic)
[![](stamps/dc_apollonian-1.png)](stamps/stamps.md#dc_apollonian)
[![](stamps/dc_booleans-1.png)](stamps/stamps.md#dc_booleans)
[![](stamps/dc_butterflies-1.png)](stamps/stamps.md#dc_butterflies)
[![](stamps/dc_cairotiles-1.png)](stamps/stamps.md#dc_cairotiles)
[![](stamps/dc_circlesblue-1.png)](stamps/stamps.md#dc_circlesblue)
[![](stamps/dc_circuits-1.png)](stamps/stamps.md#dc_circuits)
[![](stamps/dc_code-1.png)](stamps/stamps.md#dc_code)
[![](stamps/dc_ducks-1.png)](stamps/stamps.md#dc_ducks)
[![](stamps/dc_fingerprint-1.png)](stamps/stamps.md#dc_fingerprint)
[![](stamps/dc_fractaldots-1.png)](stamps/stamps.md#dc_fractaldots)
[![](stamps/dc_fractcolor-1.png)](stamps/stamps.md#dc_fractcolor)
[![](stamps/dc_gabornoise-1.png)](stamps/stamps.md#dc_gabornoise)
[![](stamps/dc_glypho-1.png)](stamps/stamps.md#dc_glypho)
[![](stamps/dc_grid3D-1.png)](stamps/stamps.md#dc_grid3D)
[![](stamps/dc_hexagons-1.png)](stamps/stamps.md#dc_hexagons)
[![](stamps/dc_hoshi-1.png)](stamps/stamps.md#dc_hoshi)
[![](stamps/dc_hyperbolictile-1.png)](stamps/stamps.md#dc_hyperbolictile)
[![](stamps/dc_inversion-1.png)](stamps/stamps.md#dc_inversion)
[![](stamps/dc_kaleidocomplex-1.png)](stamps/stamps.md#dc_kaleidocomplex)
[![](stamps/dc_kaleidoscopic-1.png)](stamps/stamps.md#dc_kaleidoscopic)
[![](stamps/dc_kaliset-1.png)](stamps/stamps.md#dc_kaliset)
[![](stamps/dc_kaliset2-1.png)](stamps/stamps.md#dc_kaliset2)
[![](stamps/dc_layers-1.png)](stamps/stamps.md#dc_layers)
[![](stamps/dc_mandala-1.png)](stamps/stamps.md#dc_mandala)
[![](stamps/dc_mandbrot-1.png)](stamps/stamps.md#dc_mandbrot)
[![](stamps/dc_mandelbox2D-1.png)](stamps/stamps.md#dc_mandelbox2D)
[![](stamps/dc_menger-1.png)](stamps/stamps.md#dc_menger)
[![](stamps/dc_moebiuslog-1.png)](stamps/stamps.md#dc_moebiuslog)
[![](stamps/dc_pentatiles-1.png)](stamps/stamps.md#dc_pentatiles)
[![](stamps/dc_poincaredisc-1.png)](stamps/stamps.md#dc_poincaredisc)
[![](stamps/dc_quadtree-1.png)](stamps/stamps.md#dc_quadtree)
[![](stamps/dc_randomoctree-1.png)](stamps/stamps.md#dc_randomoctree)
[![](stamps/dc_rotations-1.png)](stamps/stamps.md#dc_rotations)
[![](stamps/dc_spacefold-1.png)](stamps/stamps.md#dc_spacefold)
[![](stamps/dc_squares-1.png)](stamps/stamps.md#dc_squares)
[![](stamps/dc_starsfield-1.png)](stamps/stamps.md#dc_starsfield)
[![](stamps/dc_sunflower-1.png)](stamps/stamps.md#dc_sunflower)
[![](stamps/dc_tesla-1.png)](stamps/stamps.md#dc_tesla)
[![](stamps/dc_tree-1.png)](stamps/stamps.md#dc_tree)
[![](stamps/dc_triantess-1.png)](stamps/stamps.md#dc_triantess)
[![](stamps/dc_truchet-1.png)](stamps/stamps.md#dc_truchet)
[![](stamps/dc_turbulence-1.png)](stamps/stamps.md#dc_turbulence)
[![](stamps/dc_voronoise-1.png)](stamps/stamps.md#dc_voronoise)
[![](stamps/dc_vortex-1.png)](stamps/stamps.md#dc_vortex)
[![](stamps/dc_warping-1.png)](stamps/stamps.md#dc_warping)
[![](stamps/dc_worley-1.png)](stamps/stamps.md#dc_worley)
[![](stamps/glsl_acrilic-1.png)](stamps/stamps.md#glsl_acrilic)
[![](stamps/glsl_apollonian-1.png)](stamps/stamps.md#glsl_apollonian)
[![](stamps/glsl_circlesblue-1.png)](stamps/stamps.md#glsl_circlesblue)
[![](stamps/glsl_circuits-1.png)](stamps/stamps.md#glsl_circuits)
[![](stamps/glsl_code-1.png)](stamps/stamps.md#glsl_code)
[![](stamps/glsl_fractaldots-1.png)](stamps/stamps.md#glsl_fractaldots)
[![](stamps/glsl_grid3D-1.png)](stamps/stamps.md#glsl_grid3D)
[![](stamps/glsl_hoshi-1.png)](stamps/stamps.md#glsl_hoshi)
[![](stamps/glsl_hyperbolictile-1.png)](stamps/stamps.md#glsl_hyperbolictile)
[![](stamps/glsl_kaleidocomplex-1.png)](stamps/stamps.md#glsl_kaleidocomplex)
[![](stamps/glsl_kaleidoscopic-1.png)](stamps/stamps.md#glsl_kaleidoscopic)
[![](stamps/glsl_kaliset-1.png)](stamps/stamps.md#glsl_kaliset)
[![](stamps/glsl_kaliset2-1.png)](stamps/stamps.md#glsl_kaliset)
[![](stamps/glsl_mandala-1.png)](stamps/stamps.md#glsl_mandala)
[![](stamps/glsl_mandelbox2D-1.png)](stamps/stamps.md#glsl_mandelbox2D)
[![](stamps/glsl_randomoctree-1.png)](stamps/stamps.md#glsl_randomoctree)
[![](stamps/glsl_squares-1.png)](stamps/stamps.md#glsl_squares)
[![](stamps/glsl_starsfield-1.png)](stamps/stamps.md#glsl_starsfield)


## [Synth](synth/synth.md#Synth)

Variations that emulate a number of other variations, adding wavy effects using a technique from audio synthesizers.

[![](synth/synth-0.png)](synth/synth.md#synth-v2)
[![](synth/synth-1.png)](synth/synth.md#synth-v2)
[![](synth/synth-2.png)](synth/synth.md#synth-v2)
[![](synth/synth-3.png)](synth/synth.md#synth-v2)
[![](synth/synth-4.png)](synth/synth.md#synth-v2)
[![](synth/synth-5.png)](synth/synth.md#synth-v2)
[![](synth/synth-6.png)](synth/synth.md#synth-v2)
[![](synth/synth-7.png)](synth/synth.md#synth-v2)
[![](synth/synth-8.png)](synth/synth.md#synth-v2)
[![](synth/synth-9.png)](synth/synth.md#synth-v2)
[![](synth/synth-10.png)](synth/synth.md#synth-v2)
[![](synth/synth-11.png)](synth/synth.md#synth-v2)
[![](synth/synth-12.png)](synth/synth.md#synth-v2)
[![](synth/synth-13.png)](synth/synth.md#synth-v2)
[![](synth/synth-14.png)](synth/synth.md#synth-v2)
[![](synth/synth-15.png)](synth/synth.md#synth-v2)
[![](synth/synth-16.png)](synth/synth.md#synth-v2)
[![](synth/synth-17.png)](synth/synth.md#synth-v2)
[![](synth/synth-18.png)](synth/synth.md#synth-v2)
[![](synth/synth-19.png)](synth/synth.md#synth-v2)
[![](synth/synth-1001.png)](synth/synth.md#synth-v2)
[![](synth/synth-1002.png)](synth/synth.md#synth-v2)
[![](synth/synth-1003.png)](synth/synth.md#synth-v2)
[![](synth/synth-1004.png)](synth/synth.md#synth-v2)
[![](synth/synth-1005.png)](synth/synth.md#synth-v2)
[![](synth/synth-1006.png)](synth/synth.md#synth-v2)
[![](synth/synth-1007.png)](synth/synth.md#synth-v2)

## [Waves](waves/waves.md#Waves)

Variations that add waves to x, y, and/or z. There are a lot of different variants on this theme, differing in the parameters available and how they are applied.

[![](waves/auger-1.png)](waves/waves.md#auger)
[![](waves/vibration-1.png)](waves/waves.md#vibration)
[![](waves/vibration2-1.png)](waves/waves.md#vibration2)
[![](waves/waves-1.png)](waves/waves.md#waves-1)
[![](waves/waves2-1.png)](waves/waves.md#waves2)
[![](waves/waves2_3D-1.png)](waves/waves.md#waves2_3D)
[![](waves/waves2_radial-1.png)](waves/waves.md#waves2_radial)
[![](waves/waves22-1.png)](waves/waves.md#waves22)
[![](waves/waves23-1.png)](waves/waves.md#waves23)
[![](waves/waves2b-1.png)](waves/waves.md#waves2b)
[![](waves/waves2b-2.png)](waves/waves.md#waves2b)
[![](waves/waves3-1.png)](waves/waves.md#waves3)
[![](waves/waves4-1.png)](waves/waves.md#waves4)
[![](waves/waves42-1.png)](waves/waves.md#waves42)
[![](waves/waves2_wf-1.png)](waves/waves.md#wavesD2--waves2_wf)
[![](waves/waves3_wf-1.png)](waves/waves.md#wavesD3--waves3_wf)
[![](waves/waves4_wf-1.png)](waves/waves.md#wavesD4--waves4_wf)
[![](waves/wavesn-1.png)](waves/waves.md#wavesn)
[![](waves/wavesn-2.png)](waves/waves.md#wavesn)


## [Z Manipulation](zmanip/zmanip.md#Z-Manipulation)
Variations that manipulate only the z coordinate.

[![](zmanip/extrude-1.png)](zmanip/zmanip.md#extrude)
[![](zmanip/inflateZ_1-1.png)](zmanip/zmanip.md#inflateZ_1)
[![](zmanip/inflateZ_3-1.png)](zmanip/zmanip.md#inflateZ_3)
[![](zmanip/inflateZ_4-1.png)](zmanip/zmanip.md#inflateZ_4)
[![](zmanip/inflateZ_5-1.png)](zmanip/zmanip.md#inflateZ_5)
[![](zmanip/inflateZ_6-1.png)](zmanip/zmanip.md#inflateZ_6)
[![](zmanip/post_bumpmap_wf-1.png)](zmanip/zmanip.md#post_bumpmap_wf)
[![](zmanip/ztranslate-1.png)](zmanip/zmanip.md#ztranslate)
